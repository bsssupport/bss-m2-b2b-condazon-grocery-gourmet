<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CondazonGroceryGourmet
 * @author     Extension Team
 * @copyright  Copyright (c) 2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CondazonGroceryGourmet\Plugin;

/**
 * Class QuickShopHelper
 *
 * @package Bss\CondazonGroceryGourmet\Plugin
 */
class QuickShopHelper
{
    /**
     * @var \Bss\ConfiguableGridView\Helper\Data
     */
    protected $helper;

    /**
     * QuickShopHelper constructor.
     * @param \Bss\ConfiguableGridView\Helper\Data $helper
     */
    public function __construct(
        \Bss\ConfiguableGridView\Helper\Data $helper
    ) {
        $this->helper = $helper;
    }

    /**
     * Change Url quick shop
     *
     * @param object $subject
     * @param string $result
     * @param object $product
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterGetQuickViewUrl($subject, $result, $product)
    {
        $id = $product->getId();
        if ($this->helper->isEnabled()) {
            return $subject->getBaseUrl().'reorder-product/catalog_product/view/id/'.$id;
        }
        return $result;
    }
}